// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    sourceType: 'module',
    parser: '@babel/eslint-parser'
  },
  // env: {
  //   browser: true,
  //   'cypress/globals': true
  // },
  extends: [
    'plugin:vue/recommended',
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue',
    'vuetify'
    // 'cypress'
  ],
  rules: {
    "vue/max-attributes-per-line": "off",
    "no-debugger": "off",
    'vuetify/grid-unknown-attributes': 'error',
    'vuetify/no-legacy-grid': 'error',
    'vuetify/no-deprecated-classes': 'error'
  }
}
