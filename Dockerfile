FROM library/archlinux:base
EXPOSE 8080

ARG API_HOST
ENV API_HOST=${API_HOST}

RUN pacman -Syyu --noconfirm
# node-gyp is the troublemaker, it needs python2 as the default python env
# beside that it needs all these pkgs to compile:
# RUN pacman -S --noconfirm python pkgconf cairo npm grep make nodejs gcc
RUN pacman -S --noconfirm npm nodejs

RUN mkdir -p /opt/src

WORKDIR /opt/src
ADD . ./
RUN rm -fr node_modules

RUN npm install --production
RUN ./node_modules/webpack-cli/bin/cli.js --config build/webpack.config.prod.js --env API_HOST=$API_HOST
USER 1001
CMD [ "node", "server/prod-server.js"]
