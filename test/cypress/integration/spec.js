const makeAllChoices = (selectBoxList, option, index) => {
  for (option of selectBoxList[index]) option.click()
  //  option.click
  //  if(start_button is active):
  //   start_button.click
  //   do_questionaire_test
  //  if (s[index+1] exists):
  //    makeAllChoices(index+1])
}

describe('page', () => {
  const requestList = []

  after(() => {
    // In record mode, save gathered XHR data to local JSON file
    if (Cypress.env('RECORD')) {
      const path = './testfixture.json'
      cy.writeFile(path, requestList)
    }
  })
  it('title is correct', () => {
    cy.visit(Cypress.env('frontend'))
    cy.title().should('eq', 'Tutorial Anatomie')
  })
  it('resolve Bewegungsapparat urls', () => {
    cy.server({
      onRequest: (request) => {
        const url = request.url
        const method = request.method
        const data = request.body
        // We push a new entry into the xhrData array
        requestList.push({ url, method, data })
      }
    })

    //       var url = `**/bewegungsapparat/${modus}`
    //       cy.route({
    //         method: 'GET',
    //         url: url
    //       }).as('gugus')
    //       cy.wait('@gugus')
    //   cy.route({
    //     method: 'GET',
    //     url: 'http://localhost:8000/question/1',
    //     response: { image: 'http://cdn.shockers.de/out/pictures/master/product/1/abgetrenntes_bein_mit_oberschenkel-abgerissenes_bein_aus-latex-latexkoerperteile_22661.jpg',
    //       question: 'Was ist das?',
    //       options: [
    //         'Kopf',
    //         'Bein',
    //         'Arm'
    //       ]
    //     }
    //   }).as('login')
    cy.get('main .v-card').as('cards')
    cy.get('@cards').each(($card, index, $cardList) => {
      expect($cardList).to.have.length(2)
      cy.wrap($card).within(($card) => {
        cy.get('button').as('button')
      })
      cy.wrap($card).find('.v-select__slot').each(($select, index, $selectList) => {
        cy.wrap($select).click()
        cy.get('.v-menu__content.menuable__content__active')
          .find('.v-list__tile__title').should('be.visible').each(
            ($option, index, $optionList) => {
              cy.wrap($option).click({ force: true })
              cy.get('@button').click({ force: true })
            }
          )
      })
    })
  })
})
