console.log('WOOW', this)

var lastUpdateArray = []

self.addEventListener('install', event => {
  const now = Date.now()
  console.log('SW state: installing SW')
  event.waitUntil(() => {
    console.log(`Installed SW on ${now}`)
    // return self.clients.claim()
  })
})

const isOlderThan24h = (date) => {
  var age = (Date.now() - date) / 1000 / 60 / 60
  console.log(`Cache age: ${age}`)
  if (age >= 24) return true
  else return false
}

const isMediaRequest = (urlSplit) => {
  // checks url and returns true if media is requested
  var filename = urlSplit[urlSplit.length - 1].split('.')
  var fileTermination = filename[filename.length - 1]
  if (fileTermination.toLowerCase() === 'jpg' ||
      fileTermination.toLowerCase() === 'png') {
    console.log('Found media request')
    return true
  }
  return false
}

self.addEventListener('activate', async function (event) {
  // we keep one object in cache whos kwy is named after the current CACHE_VERSION
  const version = new URL(location).searchParams.get('version')
  console.log('SW state: activating with cache version:', version)
  event.waitUntil((async () => { // sw will not activate until code inhere has finished
    const keyList = await caches.keys()
    return Promise.all(keyList.map(key => {
      console.log('Travel key:', key)
      if (version !== key) {
        console.log('Deleting cache key:', key)
        return caches.delete(key)
      } else return Promise.resolve(`Lets keep cache ${version}`)
    }))
  })())
})

self.addEventListener('fetch', event => {
  console.log('Service worker fetch', event.request)
  const version = new URL(location).searchParams.get('version')
  var urlSplit = event.request.url.split('/')
  if (event.request.method === 'GET' &&
      ( // we cache json but no tests
        (event.request.headers.get('Accept') === 'application/json' &&
         urlSplit.filter(splitter => splitter === 'test').length === 0) ||
          // we also cache media files, no matter if for tests or not
          isMediaRequest(urlSplit)
      )
  ) {
    event.respondWith(async function () {
      const cachedResponse = await caches.match(event.request)
      const updateInfo = lastUpdateArray.filter(i => i.url === event.request.url)
      if (cachedResponse &&
          updateInfo.length > 0 &&
          (!isOlderThan24h(updateInfo[0].timeStamp) || isMediaRequest(urlSplit))
      ) {
        console.log(`Found response for\n ${event.request.url}`)
        console.log(cachedResponse)
        return cachedResponse
      } else {
        const response = await fetch(event.request)
        if (!response || response.status !== 200) return response
        const responseToCache = response.clone()
        try {
          const cache = await caches.open(version)
          await cache.put(event.request, responseToCache)
          if (updateInfo.length === 0) {
            lastUpdateArray.push({
              url: event.request.url,
              timeStamp: Date.now()
            })
          } else updateInfo[0].timeStamp = Date.now()
          console.log('Cache updated', lastUpdateArray)
        } catch (err) {
          console.log(`Cache open error:\n${err}`)
        }
        return response
      }
    }())
  } else {
    console.log('Skipping caching for: ', event.request)
  }
})

self.addEventListener('message', function (event) {
  // console.log('got message')
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting()
  }
})
